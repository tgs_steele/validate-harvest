#! python3
#! /usr/bin/env python3 -i

""" ======================================================================== """
# ValidateHarvest.py
# Description: Validate TGS Steele Street Harvest File
# Maintained By: Joseph Carpinelli
# Last Updated: May 12, 2019

# Todo:
# Get arguments from sys.argv[]
# Detect if ".xlsx" is at the end of the file
# Check for Valided_File_Path exists for each "HaRVesT.*" file
# Use traceback, come-on, man!
# Add a generic countErrors that takes a lambda that returns a bool
# CONSTANTS.Name
# Use main
# Fix info and debug use
# Add merged cells above data with borders
# Comments way off
# add worksheet.active_cell = "A3"
# Mislabelled to Corrected
# Newline between if and else


# STRIP Cells
# Pull cell ranges from named ranges
# Check for duplicate values
# Sort cells

# isEven
# is greater than 1400
# is numeric

# Mislabelled is not blank

# if strain are "Not Found"
# if rooms are "Not Found"

# RETURN NONE ON VOID FUNCTIONS (capitalize)

"""
def main():
    # function calling
    dictionairy()              
     
# main function calling
if __name__=="__main__":      
    main()
"""

""" ======================================================================== """


""" Imports """

# Debugging and Logging
import traceback
import time
import logging

# Utilities
import sys
import shutil
import os
import re
import pprint
import math
from copy import copy
from operator import itemgetter # , attrgetter

# Excel File Manipulation
import openpyxl
from openpyxl.utils import get_column_letter

""" End Imports """


""" Argument Validation """



""" End Argument Validation """


""" Constants """

# Format

Float_Format = "0.00"

# Regular Expressions

Room_Regex = re.compile(r"([A-S])-(\d{1,2})")

# File Extensions

Excel_Extension = ".xlsx"

# Sheetnames Used

l_Weights = "Weights"

# Row Structure

RFID_Index = 0
Weight_Index = 1
Corrected_Index = 2
Strain_Index = 3
Room_Index = 4

# Cell Ranges

Data_Row_Start = 3
Data_Row_End = 1000
Column_Start = RFID_Index + 1
Column_End = Room_Index + 1

Weights_Range = "$A$3:$E$1000"

RFID = "A3:A1000"
WEIGHT = "B3:B1000"
CORRECTED = "C3:C1000"
STRAIN = "D3:D1000"
ROOM = "E3:E1000"

# Miscellaneous

RFID_Length = 24


# Path Setup

Directory_Name = os.path.abspath(os.path.dirname(__file__))
TMP_File_Path = os.path.join(Directory_Name, "tmp.xlsx")
Safe_To_Delete_Path = os.path.join(Directory_Name, "Safe_to_DELETE_ME.txt")
# Validated_File_Path = Read_File_Path[:-5] + "_Validated.xlsx"

# Logger Constants

Log_Format = "%(asctime)s - %(levelname)s - %(message)s"
#Log_Format = "%(asctime)s - %(levelname)s:\n%(message)s\n"
Log_Filename = os.path.join(Directory_Name, "debug.log")

""" End Constants """


""" Logger Setup """

# Setup Logger

try:
    logger = logging.getLogger(__name__) # Get logger with module name

    file_handler = logging.FileHandler(Log_Filename) # Get file handler
    stream_handler = logging.StreamHandler() # Get stream handler

    formatter = logging.Formatter(Log_Format) # Get formatter

    # Handle all message levels
    logger.setLevel(logging.DEBUG)
    file_handler.setLevel(logging.DEBUG)
    stream_handler.setLevel(logging.INFO)

    # Set formatter
    file_handler.setFormatter(formatter)
    stream_handler.setFormatter(formatter)

    # Add file handlers
    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)

except:
    logger.critical("Exception!")
    logger.critical(str(traceback.format_exc()))
    # Attempt to log setup failure
    logger.warn("Logger setup failed!")
    logger.warn(str(traceback.format_exc()))

    # Notify setup failure outside logging
    print("An exception occured... Logging disabled.")

    logging.disable() # Disable logging after failed setup

# DEBUG

# Setup Argument Validation
logger.debug("Taking input instead of command arguments! -- FIX ME")

# Get User Input
logger.info("Taking user input...")
try:
    Read_File_Path = os.path.join(Directory_Name,
                              input("Enter the excel file's name: "))

except:
    # "Log" input failure
    print("Failed to receive or parse input!")
    print(str(traceback.format_exc()))
    print("Exiting program in 5 seconds...\n")
    time.sleep(5)
    sys.exit(1)

if Read_File_Path[-5:] != Excel_Extension:
    Read_File_Path += Excel_Extension

Validated_File_Path = Read_File_Path[:-5] + "_Validated.xlsx"

# DEBUG

""" End Logger Setup """


""" Functions """

# Converts in_Sheetlist None values to empty string
def convertNoneToString(in_sheetlist):
    for row in in_sheetlist:
        for cell_value in row:
            if cell_value is None:
                cell_value = str()

# Takes and openpyxl workbook and sheetname
# Returns a worksheet as a list
def getSheetlist(in_Workbook, in_Sheetname):
    Sheet = in_Workbook[in_Sheetname]

    raw_rows = list() # create a list at function scope
    out_filtered_rows = list() # create a sorted list at function scope

    for row in Sheet.iter_rows(min_row=Data_Row_Start,
                               max_row=Data_Row_End,
                               max_col=Column_End,
                               values_only=True):
        logger.debug("Adding raw row %s" %(str(row)))
        raw_rows.append(row)

    # Remove None values from the list
    for sublist in raw_rows:
        # Remove None values from sublist
        tmp_filtered_rows = [row for row in sublist if row is not None]

        # Check if any values are left
        if (len(tmp_filtered_rows) > 0) and ((sublist[0] != None) and
                                         (sublist[1] != None)):
            logger.info("Adding filtered row: %s" %(str(sublist)))
            out_filtered_rows.append(sublist) # append if list is left
            
    return out_filtered_rows

# Takes an openpyxl Workbook file and cell location information
# Capitalizes the range of cells specified in the argument parameters
# Returns a string containing the path to the capitalized file
def getUppercaseSheetlist(in_Sheetlist):
    out_sheetlist = list() # create return variable at scope
   
    for row_number, row in enumerate(in_Sheetlist, start=1):
        uppercase_row = list() # list to hold capitalized rows before appending
       
        for column_number, cell in enumerate(row, start=1):
            if cell is None:
                cell = ""
           
            uppercase_row.append(str(cell).upper()) # capitalize all characters

            logger.info("Changed %s to %s"
                        %(get_column_letter(row_number) +
                            str(column_number),
                        uppercase_row[column_number - 1]))

        out_sheetlist.append(uppercase_row)

    return out_sheetlist

# Takes a row of text from getSheetlist
# Returns a list containing the letter and number of the room
# If match is not found
# Returns a list containing an empty string and then a 0
def getRoomGroups(in_row):
    regex_match = Room_Regex.search(str(in_row[Room_Index]))

    if regex_match is not None:
        return [regex_match.group(1), int(regex_match.group(2))]
    else:
        return ["", 0]

# Takes a list containing the letter and then number of a room
# Returns the letter
f_getRoomLetter = lambda in_room_split: getRoomGroups(in_room_split)[0]

# Takes a list containing the letter and then number of a room
# Returns the number
f_getRoomNumber = lambda in_room_split: getRoomGroups(in_room_split)[1]

# Returns a sorted 2D list
def getSortedSheetlist(in_row_list):
    logger.info("Sorting by RFID as tertiary sort...")
   
    # tertiary sort by RFID, as new list
    out_sorted_rows = sorted(in_row_list, key=itemgetter(RFID_Index))

    # Sorting by room

    logger.info("Sorting by room...")

    # secondary sort, reversed
    logger.info("Sorting by room number as secondary sort...")
    out_sorted_rows.sort(key=f_getRoomNumber)
   
    # primary sort by room letter
    logger.info("Sorting by room letter as primary sort...")
    out_sorted_rows.sort(key=f_getRoomLetter)

    return out_sorted_rows

# Takes a Workbook, a Sheetname, and a 2D list to insert
# Saves 2D as an Excel Spreadsheet
# Returns None if succesful, if fail, throws exception and exits
def saveListAs(in_workbook, in_Sheetname, in_rows): # saveListAsWorkbook(in_rows):
    Sheet = in_workbook[in_Sheetname]
    """
    logger.info("Getting column styles...")
    Column_Styles = [copy(Sheet[RFID[:2]].style),
                     copy(Sheet[WEIGHT[:2]].style),
                     copy(Sheet[CORRECTED[:2]].style),
                     copy(Sheet[STRAIN[:2]].style),
                     copy(Sheet[ROOM[:2]].style)]
    """
    # Clears column C
    #Sheet.delete_cols(Corrected_Index + 1)

    # Set each item to corresponding cell
    for row_number, row in enumerate(in_rows, start=Data_Row_Start):
        for column_number, cell_value in enumerate(row, start=1):
            cell_coordinate = get_column_letter(column_number) + str(row_number)
            cell = Sheet[cell_coordinate]

            """
            # Get copies of cell styles
            if cell.has_style:
                cell_font = copy(cell.font)
                cell_border = copy(cell.border)
                cell_fill = copy(cell.fill)
                cell_number_format = copy(cell.number_format)
                cell_protection = copy(cell.protection)
                cell_alignment = copy(cell.alignment)
            """

            # skip empty values
            if (cell_value == "") or (cell_value is None):
                continue
            
            # convert value to proper data type
            if column_number == 2:
                Sheet[cell_coordinate].value = float(cell_value)
                #Sheet[cell_coordinate].number_format = Float_Format
            
            else:
                Sheet[cell_coordinate].value = cell_value

            """
            # Set cell styles
            if cell.has_style:
                cell.font = cell_font
                cell.border = cell_border
                cell.fill = cell_fill
                cell.number_format = cell_number_format
                cell.protection = cell_protection
                cell.alignment = cell_alignment
            """

            logger.info("Changed %s to %s" %(cell_coordinate, str(cell.value)))

    # Clear cells below data
    logger.info("Clearing original worksheet from row %s to row %s"
                %(len(in_rows) + Data_Row_Start, Data_Row_End - len(in_rows)))
    Sheet.delete_rows(len(in_rows) + Data_Row_Start, Data_Row_End - len(in_rows))

    # Save Validated Workbook
   
    logger.info("Saving validated spreadsheet...")
   
    try:
        in_workbook.save(Validated_File_Path)
   
    except:
        logger.critical("Exception!")
        logger.critical(str(traceback.format_exc()))
        logger.critical("Unable to save file!")
        logger.critical("The filename attempted was %s" %(Validated_File_Path))
        logger.critical("Exiting program in 5 seconds...\n")
        time.sleep(5)
        sys.exit(1)
   
    logger.info("Saved at %s" %(Validated_File_Path))

    return None

""" End Functions """


""" Main """

def main():
    logger.info("Started program...")
   
    # Load Workbook
   
    logger.info("Loading workbook %s..." %(Read_File_Path))
    try:
        spreadsheet = openpyxl.load_workbook(Read_File_Path, data_only=True)
   
    except:
        logger.critical("Exception!")
        logger.critical(str(traceback.format_exc()))
        logger.critical("Unable to load excel file!")
        logger.critical("The filename used was %s" %(Read_File_Path))
        logger.critical("Exiting program in 5 seconds...\n")
        time.sleep(5)
        sys.exit(1)
   
    logger.info("Workbook successfully loaded")

    # Convert worksheet into 2D list
    sheetlist = getSheetlist(spreadsheet, l_Weights)
    
    # Change sheet to uppercase
    capitalized_rows = getUppercaseSheetlist(sheetlist)

    # Sort
    validated_rows = getSortedSheetlist(capitalized_rows)

    """
    # Print
    print("Here is the validated list:")
    pprint.pprint(validated_rows)
    print('\n')
    """

    # Saving Validated Spreadsheet
    saveListAs(spreadsheet, l_Weights, validated_rows)

    logger.info("Exiting program in 5 seconds...\n")
    time.sleep(5)

""" End Main """


""" Call main() """
     
# main function calling
if __name__ == "__main__":      
    main()

""" End Call main() """
